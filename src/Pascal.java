import java.util.Scanner;
public class Pascal {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		//in the triangle each entry is the sum of 2 entries above it
		
		//take an integer from the user for the number of rows to display
		System.out.print("Enter a number: ");
		int rows = input.nextInt();
		
	
		//int addValues = 1;
		
		//i is the row number
		for(int i = 0; i < rows; i++)
		{	
			int values =1;
			int space = (rows - i) * 2;
			System.out.printf("%" + space + "s", " ");
			//j is the column number
			for(int j = 0; j <= i; j++)
			{
				System.out.printf("%4d", values);
				//formula to calculate for the next number
				values = values * (i - j) / (j + 1);
			}
		
			System.out.println();
	}
		}
}
